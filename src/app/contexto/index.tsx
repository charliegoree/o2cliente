import React, {createContext,useContext, useReducer} from 'react';
import { useState, useEffect, Component } from 'react';
import firebase from '../firebase';
import useDatabase from './../hooks/useDatabase';

//en el context tomar clientes y clases y horarios y cuotas, y en tiempo real manejar los cupos de los horarios nomas.

export const LoginContext = createContext();

export const StateProvider = (props:any) =>{
    const [logeado,setLogin] = useState<boolean>(false);
    const {clases,cliente,obtenerCliente,consultarTurno,leerTurno,pedirTurno,cancelarTurno,mensaje} = useDatabase();
    function setLogeado(){
        setLogin(true);
    }
    return (
        <LoginContext.Provider value={{logeado,setLogeado,clases,cliente,mensaje,obtenerCliente,consultarTurno,leerTurno,pedirTurno,cancelarTurno}}>
            {props.children}
        </LoginContext.Provider>
    )
};
export const useStateValue = () => useContext(LoginContext);