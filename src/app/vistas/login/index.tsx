import * as React from 'react';
import { Redirect } from 'react-router-dom';
import { useState, useContext} from 'react';


import style from './style.scss';
import { LoginContext } from './../../contexto/index';
import { useEffect } from 'react';

import imagen from '../../assets/logo.png';
import useDatabase from '../../hooks/useDatabase';

export default function Login(props:any){
    const {cliente,obtenerCliente} = useDatabase();
    const [login2,setLogin] = useState<string>("");
    const [input,setInput] = useState<string>("");
    const [pass,setPass] = useState<string>("thebestsnake");
    const login = useContext(LoginContext);
    useEffect(()=>{
        if (login.cliente.id != undefined){
            setPass(input);
        }
    })
    function boton(){
        setLogin(input);
        login.obtenerCliente(input); 
    }
    function change(e: React.FormEvent<HTMLInputElement>){
        setInput(e.currentTarget.value);
    }
    if (login2 == pass){
        login.setLogeado();
        return <Redirect to="/inicio" />
    } else {
        return (
            <div className={style.login}>
                <div className={style.wrapper}>
                    <img src={imagen} />
                    <p>Escribe tu celular con caracteristica sin 0 ni 15.</p>
                    <input type="text" value={input} onChange={change} placeholder='ej: 2984192419' />
                    <button onClick={boton}>Ingresar</button>
                </div>
            </div>
        );
    }
}