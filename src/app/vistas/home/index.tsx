import * as React from 'react';
import { Redirect } from 'react-router-dom';
import { useState, useContext} from 'react';
import moment from 'moment';

import style from './style.scss';
import { LoginContext } from './../../contexto/index';
import imagen from '../../assets/logo.png';
import { useEffect } from 'react';
import useDatabase from '../../hooks/useDatabase';



export default function Home(props:any){
    const login = useContext(LoginContext);
    const [dia,setDia] = useState<number>(0);
    const [hora,setHora] = useState<number>(0);
    const [clase,setClase] = useState<string>('');
    const [dias,setDias] = useState<Array<number>>([1,2,3,4,5,6,7]);
    const [fechasTurno,setFechasTurno] = useState<Array<any>>([]);
    const [horarios,setHorarios] = useState<Array<number>>([8,9,10,11,12,13,14,15,16,17,18,19,20,21,22]);
    const [primero,setPrimero] = useState<boolean>(true);
    const diasNombres = ['','lunes','martes','miercoles','jueves','viernes','sabado','domingo'];
    
    const clienteProps = login.cliente;
    useEffect(()=>{
        if (primero){
            acomodarDias();
            setPrimero(false);
            if (login.cliente.turno != ''){
                login.leerTurno(login.cliente.turno);
                console.log('lee');
            }
        }
        if (clase != '' && clase != undefined){
            switch(dia){
                case 1:
                    setHorarios(login.clases.find((c)=>c.id==clase).lunes);
                    break;
                case 2:
                    setHorarios(login.clases.find((c)=>c.id==clase).martes);
                    break;
                case 3:
                    setHorarios(login.clases.find((c)=>c.id==clase).miercoles);
                    break;
                case 4:
                    setHorarios(login.clases.find((c)=>c.id==clase).jueves);
                    break;
                case 5:
                    setHorarios(login.clases.find((c)=>c.id==clase).viernes);
                    break;
                case 6:
                    setHorarios(login.clases.find((c)=>c.id==clase).sabado);
                    break;
                case 7:
                    setHorarios(login.clases.find((c)=>c.id==clase).domingo);
                    break;
            }
        }
    })
    function acomodarDias(){
        let lista = [];
        let fechas = [];
        for (let i=0 ; i< 7 ; i++){
            let fecha = moment().add(i, 'days').toDate();
            fechas.push([fecha.getDate(),fecha.getMonth(),fecha.getFullYear()]);
            let numero = fecha.getDay();
            lista.push((numero != 0? numero : 7));
        }
        setFechasTurno(fechas);
        setDias(lista);
    }
    function pedir(){
        let id = dias.findIndex((d)=>d == dia);
        let fechaSiguiente = fechasTurno[id];
        if (login.consultarTurno(login.clases.find((c)=>c.id = clase).cupo,{cliente: login.cliente.id,clase: clase,dia: dia,dayTurno: fechasTurno[id][0],monthTurno: fechasTurno[id][1],yearTurno: fechasTurno[id][2],horaTurno: hora})){
            login.pedirTurno({
                cliente: login.cliente.id,
                clase: clase,
                dia: dia,
                dayTurno: fechasTurno[id][0],
                monthTurno: fechasTurno[id][1],
                yearTurno: fechasTurno[id][2],
                horaTurno: hora
            })
        }
    }
    function cancelar(){
        login.cancelarTurno(login.cliente.turno);
    }
    function estadoCuenta(){
        let hoy = new Date();
        let fecha = new Date(login.cliente.yearCuota,login.cliente.monthCuota,login.cliente.dayCuota);
        if (hoy > fecha){
            return <span style={{color:'red'}}>Vencida</span>
        } else {
            return <span style={{color:'green'}}>Paga</span>
        }
    }
    if (login.logueado == false || login.cliente.id == {} || login.cliente.id == undefined){
        return <Redirect to="/" />
    } else {
        return (
            <React.Fragment>
                <div className={style.cuerpo}>
                    <div className={style.cabecera}>
                        <img src={imagen} />
                        <h1>cross Training</h1>
                    </div>
                    <div className={style.contenedor}>
                        <h2>{login.cliente.nombre}</h2>
                        <p>El estado de tu cuenta: {estadoCuenta()}</p>
                        {
                            (login.cliente.turno == '' || login.cliente.turno == undefined ? 
                            <React.Fragment>
                                <div className={style.fila}>No estas anotado a ninguna clase.</div>
                                <h3>Anotarse a una Clase</h3>
                                <p>Clase a la que quiere participar:</p>
                                <div className={style.fila}>
                                    <select value={clase} onChange={(e)=> setClase(e.currentTarget.value)}>
                                        <option value=''>Elegir una Clase</option>
                                        {
                                            login.clases.map((c,key)=>{
                                                return <option key={key} value={c.id}>{c.nombre}</option>
                                            })
                                        }
                                    </select>
                                </div>
                                <p>Dia en el que se quiere presentar:</p>
                                <div className={style.fila}>
                                    <select value={dia} onChange={(e)=> setDia(parseInt(e.currentTarget.value))}>
                                        <option value={0}>Elegir un dia</option>
                                        {
                                            dias.map((d,key)=>{
                                                return <option key={key} value={d}>{diasNombres[d]}</option>
                                            })
                                        }
                                    </select>
                                </div>
                                <p>Horario</p>
                                <div className={style.fila}>
                                    <select value={hora} onChange={(e)=> setHora(parseInt(e.currentTarget.value))}>
                                        <option value={0}>Elegir un Horario</option>
                                        {
                                            horarios.map((horario,key)=>{
                                                return <option key={key} value={horario}>{(horario<10?`0${horario}:00` : `${horario}:00`)}</option>
                                            })
                                        }
                                    </select>
                                </div>
                                <div className={style.fila}>
                                    <button onClick={(e)=>pedir()}>Pedir Turno</button>
                                </div>
                        </React.Fragment>
                            : 
                        <React.Fragment>
                            <div className={style.fila}>estas anotado a la clase</div>
                            <div className={style.fila}>{login.mensaje}</div>
                            <button onClick={(e)=>cancelar()}>Cancelar</button>
                        </React.Fragment>
                            )}
                        
                    </div>
                </div>
            </React.Fragment>
        )
    }
}