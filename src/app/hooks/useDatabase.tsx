import React, { useState, useEffect } from 'react';
import firebase from '../firebase';
import { userInfo } from 'os';

interface Cliente {
    id?: string;
    nombre: string;
    apellido: string;
    telefono: string;
    cuota: string;
    dayCuota: number;
    monthCuota: number;
    yearCuota: number;
    turno:string
}
interface Cuota {
    id?: string;
    nombre: string;
    valor: number;
    cantidad: number;
}
interface Clase {
    id?:string;
    nombre: string;
    cupo: number;
    lunes: Array<number>;
    martes: Array<number>;
    miercoles: Array<number>;
    jueves: Array<number>;
    viernes: Array<number>;
    sabado: Array<number>;
    domingo: Array<number>;
}
interface Turno {
    cliente: string;
    clase: string;
    dia: number;
    dayTurno: number;
    monthTurno: number;
    yearTurno: number;
    horaTurno: number;
}

export default function useDatabase(){
    const db = firebase.firestore();
    const [cliente,setCliente] = useState<Cliente>({nombre: '',apellido: '',telefono: '',cuota: '',dayCuota: 0,monthCuota: 0,yearCuota: 0,turno: '',});
    const [clase, setClase] = useState<string>('');
    const [primera,setPrimera] = useState<boolean>(true);
    const [turnos,setTurnos] = useState<Array<Turno>>([]);
    const [clases, setClases] = useState<Array<Clase>>([]);
    const [mensaje,setMensaje] = useState<string>('');

    useEffect(()=>{
        if (primera){
            obtenerClases();
            setPrimera(false);
        }
    })
    function obtenerCliente(telefono:string){
        db.collection('clientes').where('telefono','==',telefono).onSnapshot((snap)=>{
            snap.forEach((cliente)=>{
                setCliente({
                    id: cliente.id,
                    nombre: cliente.data().nombre,
                    apellido: cliente.data().apellido,
                    telefono: cliente.data().telefono,
                    cuota: cliente.data().cuota,
                    dayCuota: cliente.data().dayCuota,
                    monthCuota: cliente.data().monthCuota,
                    yearCuota: cliente.data().yearCuota,
                    turno: cliente.data().turno
                });
            })
        })
    }
    function obtenerTurnos(dia:number,mes:number,ano:number,hora:number){
        db.collection('turnos').where('clase',"==",clase).where('dayTurno','==',dia).where('monthTurno','==',mes).where('yearTurno','==',ano).where('horaTurno',"==",hora).onSnapshot((snap)=>{
            let lista:Array<any> = [];
            snap.forEach((turno)=>{
                lista.push(turno);
            })
            setTurnos(lista);
        })
    }
    function obtenerClases(){
        db.collection('clases').onSnapshot((snap)=>{
            let lista: Array<Clase> = [];
            snap.forEach((clase)=>{
                lista.push({
                    id:clase.id,
                    nombre:clase.data().nombre,
                    cupo:clase.data().cupo,
                    lunes: clase.data().lunes,
                    martes: clase.data().martes,
                    miercoles: clase.data().miercoles,
                    jueves: clase.data().jueves,
                    viernes: clase.data().viernes,
                    sabado: clase.data().sabado,
                    domingo: clase.data().domingo
                });
            });
            setClases(lista);
        })
    }
    function horita(n:number){
        return (n < 10? `0${n}:00` : `${n}:00`)
    }
    function leerTurno(id:string){
        db.collection('turnos').doc(id).get().then((turno)=>{
            let fecha = new Date();
            let fechaTurno = new Date(turno.data().yearTurno,turno.data().monthTurno,turno.data().dayTurno);
            if (fecha > fechaTurno && fecha.getDate() != turno.data().dayTurno){
                console.log(fecha.toDateString()+" > "+fechaTurno.toDateString());
                cancelarTurno(id);
            } else {
                console.log(fechaTurno.toDateString());
                setMensaje(horita(turno.data().horaTurno) + ' - ' + turno.data().dayTurno +'/'+ turno.data().monthTurno +'/'+ turno.data().yearTurno)
            }
        })
    }
    function consultarTurno(cupo:number,turno:Turno){
        let lista: Array<any> = [];
        turnos.forEach((t)=>{
            if (t.clase == turno.clase && t.horaTurno == turno.horaTurno){
                lista.push(turno.clase);
            }
        })
        if (lista.length < cupo){
            return true;
        } else {
            return false;
        }
    }
    function pedirTurno(turno:Turno){
        db.collection('turnos').add({
            cliente: turno.cliente,
            clase: turno.clase,
            dia: turno.dia,
            dayTurno: turno.dayTurno,
            monthTurno: turno.monthTurno,
            yearTurno: turno.yearTurno,
            horaTurno: turno.horaTurno
        }).then((docRef)=>{
            db.collection('clientes').doc(turno.cliente).update({
                turno: docRef.id
                }
            )
        })
    }
    function cancelarTurno(turno:string){
        db.collection('turnos').doc(turno).delete().then(()=>{
        })
        db.collection('clientes').doc(cliente.id).update({turno:''}).then(()=>{
        });
    }

    return {mensaje,cliente,clase,clases,consultarTurno,pedirTurno,leerTurno,cancelarTurno,obtenerCliente,obtenerTurnos,obtenerClases};
}