export interface Cuota {
    id?: string;
    nombre: string;
    duracion: number;
    valor: number;
}
export interface Cliente {
    id?: string;
    nombre: string;
    telefono: string;
    cuota: string;
    fecha: Date;
    anotado: string;
}
export interface Clase {
    id?:string;
    nombre:string;
    cupo:number;
}
export interface Horario{
    id?: string;
    dia: number;
    hora: string;
    clases: Array<string>;
    cupo:number;
    clientes: Array<string>;
}
