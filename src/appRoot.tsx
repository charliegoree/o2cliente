import * as React from 'react';
import { Component } from 'react';
import { render } from 'react-dom';
import { BrowserRouter } from "react-router-dom";
import { Rutas } from './app/rutas';


import style from './app/style.scss';
import { StateProvider } from './app/contexto';

class App extends Component<{},{}>{
  render() {
    return (
          <StateProvider>
          <div className={style.base}>
            <BrowserRouter>
                <Rutas />
            </BrowserRouter>
          </div>
          </StateProvider>
    )
  }
}

render(<App />, document.getElementById('root'));
